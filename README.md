# Install
-------
	1. venv
	2. pip install -r requirements.txt

# Run
-------
- python client.py [source_dir] [host] [port]
- python server.py [dest_dir] [host] [port]


# Algo
-------
1. watchdog for filesystem monitorings
2. queue for collecting all events, only one consumer and uploaded (one thread to rule them all :) )
2. sockets for data transfering
3. 1 sec lag to sync data
4. fixed chunk size for file transfering


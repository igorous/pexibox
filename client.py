import sys
import time
import logging
from socket import socket
from Queue import Queue
import pickle
import threading
import os
import base64
import struct

from common import Package, BUFFER_SIZE, read_in_chunks, canonize_path, PACKAGE_PREFIX, LEN_PREFIX_SIZE
from watchdog.observers import Observer
from watchdog.events import * #bad practice
from watchdog.observers.api import EventQueue

class Monitor(FileSystemEventHandler):
    def __init__(self, dir_path, host, port):
        self.dir_path = canonize_path(dir_path)
        self.sock = socket()
        self.sock.connect((host, int(port)))
        self.queue = Queue()
        self.interval = 1

        thread = threading.Thread(target=self.watch, args=())
        thread.daemon = True
        thread.start()

    def send_package(self, package):
        content = pickle.dumps(package)
        self.sock.sendall(struct.pack(PACKAGE_PREFIX, len(content)) + content)

    def watch(self):
        while True:
            if not self.queue.empty():
                try:
                    next_event = self.queue.get()
                    package = Package(next_event, self.dir_path) 
                    self.send_package(package)

                    if not next_event.is_directory:
                        if next_event.event_type == EVENT_TYPE_CREATED or next_event.event_type == EVENT_TYPE_MODIFIED:
                                with open(next_event.src_path, 'rb') as inp:
                                    for chunk in read_in_chunks(inp, BUFFER_SIZE):
                                        self.send_package(Package(None, None, True, chunk))
                                
                except Exception as E:
                    print "watch exception:", E, next_event

            time.sleep(self.interval)
        
    def on_any_event(self, event):
        if not event.src_path.endswith('.DS_Store'):
            self.queue.put(event)

    def stop(self):
        self.sock.close()


if __name__ == "__main__":
    path = sys.argv[1]
    event_handler = Monitor(path, sys.argv[2], sys.argv[3])
    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        pass
    except Exception as E:
        print "Execution stopped due to exception:" + str(E)
    finally:
        event_handler.stop()
        observer.stop()
        observer.join()

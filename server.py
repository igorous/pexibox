from socket import socket
import sys
import pickle
import time
import shutil
import base64
import struct

from common import Package, BUFFER_SIZE, canonize_path, PACKAGE_PREFIX, LEN_PREFIX_SIZE
from watchdog.events import * #bad practice


def touch(path):
    open(path, 'a').close()

def has_same_suffix(old_path, new_path):
    return old_path.split('/')[-1] == new_path.split('/')[-1]

class Server():
    def __init__(self, path, host, port):
        self.root_path = canonize_path(path)
        self.active_file = None
        self.interval = 0.1

        self.sock = socket()
        self.sock.bind((sys.argv[2], int(sys.argv[3])))
        self.sock.listen(1)

        self.conn, addr = self.sock.accept()

    def fullpath(self, path):
        return self.root_path + path

    def process_event(self, event):
        event_type = event.event_type
        if event_type == EVENT_TYPE_CREATED:
            if event.is_directory:
                os.mkdir(self.fullpath(event.src_path))
            else:
                self.active_file = event.src_path
                touch(self.fullpath(self.active_file))

        elif event_type == EVENT_TYPE_MODIFIED:
            if not event.is_directory:
                # no rewriting file, plain delete and create
                self.delete(event.src_path)
                self.active_file = event.src_path
                
        elif event_type == EVENT_TYPE_MOVED:
            if has_same_suffix(event.src_path, event.dst_path):
                return

            shutil.move(self.fullpath(event.src_path), self.fullpath(event.dst_path))
        elif event_type == EVENT_TYPE_DELETED:
            self.delete(event.src_path, event.is_directory)

    def delete(self, path, is_dir = False):
        try:
            if is_dir:
                shutil.rmtree(self.fullpath(path))
            else:
                os.remove(self.fullpath(path))

        except Exception as E:
            # should be concrete exception
            # skip file if not exists
            pass

    def receive_file_chunk(self, chunk):
        with open(self.root_path + self.active_file, 'ab') as outp:
            outp.write(chunk)

    def receive_package(self):
        raw_msglen = self.recvall(LEN_PREFIX_SIZE)
        if not raw_msglen:
            return None
        msglen = struct.unpack(PACKAGE_PREFIX, raw_msglen)[0]
        return self.recvall(msglen)

    def recvall(self, length):
        data = b''
        while len(data) < length:
            packet = self.conn.recv(length - len(data))
            if not packet:
                return None
            data += packet
        return data

    def run(self):
        try:
            while True:
                data = self.receive_package()
                if not data:
                    break
                try:
                    event = pickle.loads(data)
                    if event.keep_transfer:
                        self.receive_file_chunk(event.data)
                    else:
                        self.process_event(event)

                except IOError as IO:
                    print "IOError", IO
                    pass
                except Exception as E:
                    print "server run exception:", E

                time.sleep(self.interval)

        except KeyboardInterrupt:
            print "Server shutdown"
        except Exception as E:
            print "Server execution stopped due to exception:" + str(E)
        finally:
            self.sock.close()
        

if __name__ == "__main__":
    path = sys.argv[1] if len(sys.argv) > 1 else '.'
    s = Server(path, sys.argv[2], sys.argv[3])
    s.run()

import os


BUFFER_SIZE = 8 * 1024
PACKAGE_PREFIX = '>I'
LEN_PREFIX_SIZE = 4


def remove_prefix(string, prefix):
    return string[len(prefix):] if string.startswith(prefix) else string

def read_in_chunks(file_object, chunk_size=BUFFER_SIZE):
    while True:
        data = file_object.read(chunk_size)
        if not data:
            break
        yield data

def canonize_path(path):
    path = os.path.abspath(path)
    if not path.endswith('/'):
        path += '/'

    return path

class Package():
    def __init__(self, event, prefix, keep_transfer = False, data = None):
        self.src_path = None
        self.dst_path = None
        self.event_type = None
        self.is_directory = False

        if event is not None:
            self.event_type = event.event_type
            self.is_directory = event.is_directory

            if prefix is not None:
                self.src_path = remove_prefix(event.src_path, prefix) if hasattr(event, 'src_path') else None
                self.dst_path = remove_prefix(event.dest_path, prefix) if hasattr(event, 'dest_path') else None

        self.keep_transfer = keep_transfer
        self.data = data


    def to_str(self):
        return "%s %s %s %s %s %s" % (self.event_type, self.src_path, self.dst_path, \
                                         self.keep_transfer, (self.data is not None), self.is_directory)
        
